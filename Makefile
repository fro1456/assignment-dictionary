f = felf64
linker = ld
compiler = nasm

all: lib.o main.o dict.o
	$(linker) -o lab2 lib.o main.o dict.o
lib.o: lib.asm
	$(compiler) -f $(f) -o lib.o lib.asm
main.o: main.asm
	$(compiler) -f $(f) -o main.o main.asm
dict.o: dict.asm
	$(compiler) -f $(f) -o dict.o dict.asm
clean:
	rm -f lib.o main.o dict.o lab2
