section .text
%define r 8
global find_word

extern string_equals
extern print_char

find_word:
	.cnt:
		push rdi
		push rsi
		add rsi, r
		call string_equals
		pop rsi
		pop rdi
		test rax,1
		jz .pos
		mov rsi,[rsi];we take the value of rsi
		test rsi,0
		jz .neg
		jmp .cnt
		
	.pos:
		mov rax,rsi
		ret
	.neg:
		mov rax,0
		ret
	
	
