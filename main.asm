%include "colon.inc"
%define r 8

extern read_word
extern print_string
extern find_word
extern string_length
extern exit

section .rodata

%include "words.inc"
	error: db 'error:can not find your data', 0xA, 0
	too_long: db 'error:data is too long', 0xA, 0
buffer: times 255 db 0

section .text


global _start

_start:
	mov rdi,buffer
	call read_word
	test rax,0
	jz .too_long
	
	
	call .find_word_func
	test rax,0
	jz .error
	
.find_word_by_key:
	mov rdi,rax
	add rdi,r;num of bytes
	;push rdi;in case it can be changed
	;call string_length
	;pop rdi
	;add rdi,rax
	add rdi, rdx
	inc rdi
	call print_string
	xor rdi, rdi
	call exit
	
	
.find_word_func:
	mov rsi,val
	mov rdi,rax
	call find_word
	ret
.too_long:
	mov rdi, too_long
	jmp error2
	
.error:
	mov rdi, error
	.error2:
		call string_length
		mov rdx, rax
		mov rax, 1
		mov rdi, 2
		mov rsi, error
		syscall
		call exit
